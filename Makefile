# Makefile

# The make commands are: 

# Which compiler
CC = gcc

# Where to install
INSTDIR = /usr/local/bin
LIBINSTDIR = /usr/lib

# Local Libraries
# MYLIB = 

# Where are include files kept
# INCLUDE =

# All the source files which electrotest depends on.
FILES = main.c libresistance.so libpower.so libcomponent.so

# Options for development (optimization level 0, debug, position independent and all warnings)
CFLAGS = -O0 -g -fPIC -Wall
# Options for release (Optimization level 2 and ansi??)
# CFLAGS = -O2 –ansi


all: forkExec threads socket cdrom-eject print-arg-list

forkExec: forkExec.c
	$(CC) $(CFLAGS) forkExec.c -o forkExec;

threads: threads.c
	# Requires library POSIX threads - libpthread.h
	$(CC) $(CFLAGS) threads.c -o threads -lpthread;

primes: primes.c
	# Requires library POSIX threads - libpthread.h
	$(CC) $(CFLAGS) primes.c -o primes -lpthread;

socket: socket-server.c socket-client.c
	$(CC) $(CFLAGS) socket-server.c -o socket-server;
	$(CC) $(CFLAGS) socket-client.c -o socket-client;

cdrom-eject: cdrom-eject.c
	$(CC) $(CFLAGS) cdrom-eject.c -o cdrom-eject;

print-arg-list: print-arg-list.c
	$(CC) $(CFLAGS) print-arg-list.c -o print-arg-list;


## Clean
c: 		clean
claen: 		clean
calean: 	clean
calaen: 	clean
clean:
	@echo "\nRensar upp..."
	@-rm -f \
	forkExec \
	threads \
	primes \
	socket-server \
	socket-client \
	print-arg-list \
	cdrom-eject \
	*o \
	*a \
	*so \
	*_test;
	@echo "\nKlart! :)\n"



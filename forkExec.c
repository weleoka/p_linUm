#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>

/* Globals */
sig_atomic_t child_exit_status;


/* Spawn a child process running a new program. PROGRAM is the name
of the program to run; the path will be searched for this program.
ARG_LIST is a NULL-terminated list of character strings to be
passed as the program’s argument list. Returns the process ID of
the spawned process. */
int spawn (char* program, char** arg_list)
{
    pid_t child_pid;

    /* Duplicate this process. */
    child_pid = fork ();

    if (child_pid != 0)
        /* This is the parent process. */
        return child_pid;
    else {
        /* Now execute PROGRAM, searching for it in the path. */
        execvp (program, arg_list);
        /* The execvp function returns only if an error occurs. */
        fprintf (stderr, "an error occurred in execvp\n");
        abort ();
    }
}

void clean_up_child_process (int signal_number)
{
    /* Clean up the child process. */
    printf ("CALL: clean_up_child_process\n");
    int status;
    wait (&status);
    /* Store its exit status in a global variable. */
    child_exit_status = status;
}

int main ()
{
    int child_status;
    /* The argument list to pass to the “ls” command. */
    char* arg_list[] = {
    "ls",
    /* argv[0], the name of the program. */
    "-l",
    "/home/bizles",
    NULL
    /* The argument list must end with a NULL. */
    };

    /* Handle SIGCHLD by calling clean_up_child_process. */
    struct sigaction sigchld_action;
    memset (&sigchld_action, 0, sizeof (sigchld_action));
    sigchld_action.sa_handler = &clean_up_child_process;
    sigaction (SIGCHLD, &sigchld_action, NULL);

    /* Spawn a child process running the “ls” command.
    returns child process ID. */
    spawn ("ls", arg_list);
    /* Wait for the child process to complete. */
    wait (&child_status);
    if (WIFEXITED (child_status))
        printf ("the child process exited normally, with exit code %d\n",
            WEXITSTATUS (child_status));
    else
        printf ("the child process exited abnormally\n");

    printf ("done with main program\n");
    return 0;
}


/*
The exec functions replace the program running in a process with another program.
When a program calls an exec function, that process immediately ceases executing that
program and begins executing a new program from the beginning, assuming that the
exec call doesn’t encounter an error.

Within the exec family, there are functions that vary slightly in their capabilities
and how they are called.

Functions that contain the letter p in their names ( execvp and execlp ) accept a
program name and search for a program by that name in the current execution
path; functions that don’t contain the p must be given the full path of the pro-
gram to be executed.

Functions that contain the letter v in their names ( execv , execvp , and execve )
accept the argument list for the new program as a NULL-terminated array of
pointers to strings. Functions that contain the letter l ( execl , execlp , and
execle ) accept the argument list using the C language’s varargs mechanism.

Functions that contain the letter e in their names ( execve and execle ) accept an
additional argument, an array of environment variables.The argument should be
a NULL-terminated array of pointers to character strings. Each character string
should be of the form “ VARIABLE=value ”.

Because exec replaces the calling program with another one, it never returns unless an
error occurs.

When you use execv execvp or execve pass the name of the invoking function
as the first argument argv[0], just like the shell does.*/

/*
Assign a higher niceness value to give a program less priority:
% nice -n 10 sort input.txt > output.txt
Note that only a process with root privilege can run a process with a negative nice-
ness value or reduce the niceness value of a running process.*/

/*
The waitpid function can be used to wait for a specific child process to exit
instead of any child process.The wait3 function returns CPU usage statistics
about the exiting child process, and the wait4 function allows you to specify 
additional options about which processes to wait for.*/